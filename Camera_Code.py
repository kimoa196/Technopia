'''
------------------------------------Technopia Team----------------------------------



'''
import face_recognition
import cv2
import time
import threading
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import glob 
import os
import subprocess

def callMotion ():
	#subprocess.call("sudo su","roottoor--++",shell=True)
	mo=subprocess.call("motion",shell=True)
	if mo !=0:
		return main ("Erorr check your connection")

def get_last_file():
	list_of_files=glob.glob('data/*')
	latest_file=max(list_of_files,key=os.path.getctime)
	return latest_file

def camera_fun():
	
	video_capture = cv2.VideoCapture(0)
	known_face_encodings=[]
	face_locations=[]
	face_encodings=[]
	face_names=[]
	process_this_frame=[]
	count=0
	pastTime = time.time() - 100
	while True:
		ret,frame=video_capture.read(0)
		small_frame=cv2.resize(frame,(0,0), fx=0.25,fy=0.25)
		rgb_small_frame = small_frame[:, :, ::-1]
		if process_this_frame:
			face_locations=face_recognition.face_locations(rgb_small_frame)
			face_encodings=face_recognition.face_encodings(rgb_small_frame,face_locations)
			for n in face_encodings :
				now=time.strftime('%y%m%d%H%M%S')
				cv2.imwrite(("data/"+str(now)+'.jpg'),frame)
				nowtime = time.time()
				#if (time.time() - nowtime) > 12:
				if time.time() - pastTime > 60:
					mail_fun()
					pastTime = time.time()
				
			face_names=[]
			for face_encoding in face_encodings:
				matches=face_recognition.compare_faces(known_face_encodings,face_encodings)
				name="Unknown"
				if True in matches :
					first_match_index=matches.index(True)
					name=known_face_encodings[first_match_index]
				face_names.append(name)
		process_this_frame=not process_this_frame
		for (top, right, bottom, left), name in zip(face_locations, face_names):
			top *= 4
			right *= 4
			bottom *= 4
			left *= 4
			cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
			cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), -1)
			font = cv2.FONT_HERSHEY_DUPLEX
			cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
		
		cv2.imshow('Video', frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break
	video_capture.release()
	cv2.destroyAllWindows()


def mail_fun():
	email_user = '***********'
	email_password = '*****'
	email_send = '******'

	subject = 'subject'

	msg = MIMEMultipart()
	msg['From'] = email_user
	msg['To'] = email_send
	msg['Subject'] = subject

	body = 'Hi there, sending this email from Technobia !'
	msg.attach(MIMEText(body,'plain'))

	filename=get_last_file()
	attachment  =open(get_last_file(),'rb')

	part = MIMEBase('application','octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition',"attachment; filename= "+filename)

	msg.attach(part)
	text = msg.as_string()
	server = smtplib.SMTP('smtp.gmail.com',587)
	server.starttls()
	server.login(email_user,email_password)
	server.sendmail(email_user,email_send,text)
	server.quit()


#camera_fun()



if __name__=='__main__':
	
	t1= threading.Thread(target=callMotion, args=()).start()
	t2= threading.Thread(target=camera_fun(), args=()).start()
	
	
