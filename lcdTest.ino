#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

LiquidCrystal lcd(12,11,5,4,3,2);

#define led 8
char serialA;

//SoftwareSerial bluetoothSerial(15,14);

char text[100];
char textCounter = 0;
boolean newText = 0;


void setup () {
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  lcd.begin(16,2);
  
  lcd.print("123456789123456789");
  delay(1000);
  lcd.clear();
}


void loop() {
  
  while (Serial.available() > 0) {
  
  serialA = Serial.read();
  text[textCounter] = serialA;
  textCounter++;
  digitalWrite(led,HIGH);
  delay(100);
  newText = 1;
  }
  
  if (newText) {
  textCounter++;
  text[textCounter] = '\0';
  textCounter = 0;
  while (text[textCounter] != '\0') {
  
  lcd.print(text[textCounter]);
  if (textCounter%16 == 0 && textCounter !=0) {lcd.clear();}
  //lcd.scrollDisplayLeft();
  textCounter++;
  delay(150);
  }
  
  }
  
  textCounter = 0;
  newText = 0;
  digitalWrite(led,LOW);
  lcd.clear();
}
