#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

void chCopy(char small[],char big[],char start) {

  char smallCounter = 0;
 
  while (smallCounter <= 15 && big[start] != '\0') {
  small[smallCounter] = big[start];
  smallCounter++;
  start++;
  }
  smallCounter++;
  small[smallCounter]='\0';
}

LiquidCrystal lcd(12,11,5,4,3,2);
SoftwareSerial bluetoothSerial(15, 14); // RX, TX
#define led 8
char serialA;

//SoftwareSerial bluetoothSerial(15,14);

char text[100];
char displayText[17];
char textCounter = 0;
boolean newText = 0;


void setup () {
  Serial.begin(9600);
  pinMode(led,OUTPUT);
  lcd.begin(16,2);
  bluetoothSerial.begin(9600);
  lcd.print("WELCOME");
  delay(1000);
  lcd.clear();
}


void loop() {
  
  while (bluetoothSerial.available() > 0) {
  
  serialA = bluetoothSerial.read();
  text[textCounter] = serialA;
  textCounter++;
  digitalWrite(led,HIGH);
  delay(100);
  newText = 1;
  //textCounter++;
  //text[textCounter] = '\0';
  }
  
  if (newText) {
  Serial.println(text);
  char displayCounter = 0;
  
  if (textCounter < 16) {lcd.print(text);
    delay(2000);
    }
  else {
  while (displayCounter <= (textCounter-16)) {
  chCopy(displayText,text,displayCounter);
  
  lcd.print(displayText);
  delay(350);
  displayCounter++;
  lcd.clear();
  } }
  
  }
  
  textCounter = 0;
  newText = 0;
  digitalWrite(led,LOW);
  lcd.clear();
}
